//
//  ViewController.swift
//  Spotify
//
//  Created by Luca Palmese on 04/12/2019.
//  Copyright © 2019 Luca Palmese. All rights reserved.
//

import UIKit
import CoreML
import SoundAnalysis
import AVFoundation

class HomeViewController: UIViewController {
    
    // Sync Boolean
    var isDetectionStarted = false
    
    // Instructions Label Outlet
    @IBOutlet weak var instructionsLabel: UILabel!
    
    // Spotify Button Outlet
    @IBOutlet weak var spotifyButton: UIButton!
    
    // Spotify Image Outlet
    @IBOutlet weak var spotifyImage: UIImageView!
    
    // Spotify Image Pulsator
    let pulsator = Pulsator()
    
    // Settings Button Outlet
    @IBOutlet weak var settingsButton: UIButton!
    
    // x Button Outlet
    @IBOutlet weak var xButton: UIButton!
    
    // TableView Outlet
    @IBOutlet weak var tableView: UITableView!
    
    // Audio Engine
    var audioEngine = AVAudioEngine()
    
    // Streaming Audio Analyzer
    var streamAnalyzer: SNAudioStreamAnalyzer!
    
    // Serial dispatch queue used to analyze incoming audio buffers.
    let analysisQueue = DispatchQueue(label: "com.apple.AnalysisQueue")
    
    var resultsObserver: ResultsObserver!
    
    // No Result Outlets
    @IBOutlet weak var circle: UIImageView!
    @IBOutlet weak var sorryLabel: UILabel!
    @IBOutlet weak var weDidntQuiteCatchThatLabel: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    
    
    // Defining titles array
    var titles: [String] = ["Recently played", "Your heavy rotation", "Your top 10 artists", "Uniquely yours", "Jump back in", "Your favorite albums and songs", "Playlists you shouldn't miss", "Based on your recent listening", "Made for you", "Recommended for today"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// SET SPOTIFY SOUND RECOGNITION
        setAudioRecognition()
        
        // NOTIFICATION PATTERN to understand when the DetectedSongViewController Modal is dismissed
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomeViewController.handleModalDismissed),
                                               name: NSNotification.Name(rawValue: "DetectedSongModalIsDismissed"),
                                               object: nil)
        
        // NOTIFICATION PATTERN to understand when the Song Is Detected, evoking the function 'endDetection' pasting the Song Title to the Singleton
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomeViewController.endDetection),
                                               name: NSNotification.Name(rawValue: "SongIsDetected"),
                                               object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden =  true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.circle.isHidden = true
        self.pulsator.position = CGPoint(x: self.spotifyImage.center.x, y: self.spotifyImage.center.y)
        
        UIView.animate(withDuration: 2.0) {
            self.instructionsLabel.frame = CGRect(x: 53, y: 53, width: 143, height: 26)
            self.instructionsLabel.font = self.instructionsLabel.font.withSize(15)
            self.instructionsLabel.alpha = 1
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.settingsButton.alpha = 1
        self.tabBarController?.tabBar.alpha = 1
        self.pulsator.stop()
        self.pulsator.removeFromSuperlayer()
        self.stopAudioEngine()
        
        // Reset Instruction Label
        self.instructionsLabel.frame = CGRect(x: 20, y: 53, width: 26, height: 26)
        self.instructionsLabel.font = self.instructionsLabel.font.withSize(1)
        self.instructionsLabel.alpha = 0
    }
    
    // Function to hide the Status Bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    // Shazam Button Tapped Action
    @IBAction func spotifyButtonTapped(_ sender: UIButton) {
        
        // Detection is started
        self.isDetectionStarted = true
        
        // Disabling User Interaction
        self.spotifyButton.isUserInteractionEnabled = false
        self.instructionsLabel.isHidden = true
        self.spotifyImage.isHidden = false
        self.spotifyButton.isHidden = true
        self.settingsButton.isHidden = true
        self.xButton.isHidden = false
        self.tableView.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        
        
        /// Shazam Button Animation & underneath items FadeOut
        UIView.animate(withDuration: 1.0) {
            
            self.setNeedsStatusBarAppearanceUpdate()
            
            /// Shazam Button Image Changing & Scaling + Shadow Configuration
            self.spotifyImage.image = UIImage(named: "spotify")
            self.spotifyImage.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
            let scaleTransform = CGAffineTransform(scaleX: 8.5, y: 8.5)
            self.spotifyImage.transform = scaleTransform
            self.spotifyImage.layer.shadowColor = UIColor.black.cgColor
            self.spotifyImage.layer.shadowOffset = CGSize(width: 0.6, height: 0.6)
            self.spotifyImage.layer.shadowOpacity = 1
            self.spotifyImage.layer.shadowRadius = 1.0
            self.spotifyImage.clipsToBounds = false
            
            // Reset Instruction Label
            self.instructionsLabel.frame = CGRect(x: 20, y: 53, width: 26, height: 26)
            self.instructionsLabel.font = self.instructionsLabel.font.withSize(1)
            self.instructionsLabel.alpha = 0
            
            /// Pulse
            let pulseAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            pulseAnimation.duration = 1.5
            pulseAnimation.toValue = 6.8
            pulseAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            pulseAnimation.autoreverses = true
            pulseAnimation.repeatCount = .greatestFiniteMagnitude
            self.spotifyImage.layer.add(pulseAnimation, forKey: "layerAnimation")
            
            /// Waves Translation Animation
            let translationAnimation: CABasicAnimation = CABasicAnimation(keyPath: "position")
            translationAnimation.duration = 1.0
            translationAnimation.toValue = [self.view.center.x, self.view.center.y]
            translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            translationAnimation.fillMode = CAMediaTimingFillMode.forwards;
            translationAnimation.isRemovedOnCompletion = false;

            /// Waves Animation
            self.pulsator.numPulse = 5
            self.pulsator.animationDuration = 9.0
            self.view.layer.addSublayer(self.pulsator)
            self.view.bringSubviewToFront(self.spotifyImage)
            self.pulsator.start()
            self.pulsator.add(translationAnimation, forKey: "pulsator")
            self.pulsator.add(pulseAnimation, forKey: "layerAnimation")
            self.pulsator.radius = 100.0
        }

        /// SPOTIFY SOUND RECOGNITION & Resets ResultLabel
        startAudioEngine()
    }
    
    
    // x Button Tapped Action
    @IBAction func xButtonTapped(_ sender: UIButton) {
        
        /// Detection stopped
        self.isDetectionStarted = false
        
        /// Enabling User Interaction
        self.spotifyButton.isUserInteractionEnabled = true
        self.instructionsLabel.isHidden = false
        self.spotifyImage.isHidden = true
        self.spotifyButton.isHidden = false
        self.settingsButton.isHidden = false
        self.xButton.isHidden = true
        self.tableView.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        
        /// Instruction Label reappearing and animating
        UIView.animate(withDuration: 2.0) {
            self.instructionsLabel.frame = CGRect(x: 53, y: 53, width: 143, height: 26)
            self.instructionsLabel.font = self.instructionsLabel.font.withSize(15)
            self.instructionsLabel.alpha = 1
        }
        
        /// Stop Pulsing
        self.pulsator.save()
        self.pulsator.stop()
        
        /// Shazam Button Image Changing & Scaling + Shadow Configuration
        self.spotifyImage.image = UIImage(named: "spotify")
        self.spotifyImage.center = CGPoint(x: self.spotifyButton.center.x, y: self.spotifyButton.center.y)
        let scaleTransform = CGAffineTransform(scaleX: 1 / 8.5, y: 1 / 8.5)
        self.spotifyImage.transform = scaleTransform
        self.spotifyImage.layer.shadowColor = UIColor.clear.cgColor
        
        /// Hide No Result Items
        self.circle.isHidden = true
        self.sorryLabel.isHidden = true
        self.weDidntQuiteCatchThatLabel.isHidden = true
        self.tryAgainButton.isHidden = true
        
        /// Stop AudioEngine and Deletes Result String
        self.stopAudioEngine()
        Session.sharedInstance.detectedSong = ""
        
    }
    
    
    @IBAction func tryAgainButtonTapped(_ sender: UIButton) {
        
        /// Hide No Result Items
        self.circle.isHidden = true
        self.sorryLabel.isHidden = true
        self.weDidntQuiteCatchThatLabel.isHidden = true
        self.tryAgainButton.isHidden = true
        self.spotifyImage.isHidden = false
        
//        /// Resets Spotify Image dimensions
//        let scaleTransform = CGAffineTransform(scaleX: 1 / 8.5, y: 1 / 8.5)
//        self.spotifyImage.transform = scaleTransform
        
        /// Shazam Button Animation & underneath items FadeOut
        UIView.animate(withDuration: 1.0) {
            
            self.setNeedsStatusBarAppearanceUpdate()
            
            /// Shazam Button Image Changing & Scaling + Shadow Configuration
            self.spotifyImage.image = UIImage(named: "spotify")
            self.spotifyImage.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
            let scaleTransform = CGAffineTransform(scaleX: 8.5, y: 8.5)
            self.spotifyImage.transform = scaleTransform
            self.spotifyImage.layer.shadowColor = UIColor.black.cgColor
            self.spotifyImage.layer.shadowOffset = CGSize(width: 0.6, height: 0.6)
            self.spotifyImage.layer.shadowOpacity = 1
            self.spotifyImage.layer.shadowRadius = 1.0
            self.spotifyImage.clipsToBounds = false
            
            /// Pulse
            let pulseAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            pulseAnimation.duration = 1.5
            pulseAnimation.toValue = 6.8
            pulseAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            pulseAnimation.autoreverses = true
            pulseAnimation.repeatCount = .greatestFiniteMagnitude
            self.spotifyImage.layer.add(pulseAnimation, forKey: "layerAnimation")
        }

        /// Starts Pulsing again
        self.pulsator.start()
        
        self.startAudioEngine()
        Session.sharedInstance.detectedSong = ""

    }
    
    
    
    // SET SPOTIFY SOUND RECOGNITION
    func setAudioRecognition() {
        /// SoundClassifier definition
        let spotifyClassifier = SpotifySoundClassifier()
        let model: MLModel = spotifyClassifier.model
        
        /// Get the native audio format of the engine's input bus.
        let inputFormat = self.audioEngine.inputNode.inputFormat(forBus: 0)
        
        /// Create a new stream analyzer.
        streamAnalyzer = SNAudioStreamAnalyzer(format: inputFormat)
        
        /// Create a new observer that will be notified of analysis results.
        /// Keep a strong reference to this object.
        resultsObserver = ResultsObserver()
        
        do {
            // Prepare a new request for the trained model.
            let request = try SNClassifySoundRequest(mlModel: model)
            try streamAnalyzer.add(request, withObserver: resultsObserver)
        } catch {
            print("Unable to prepare request: \(error.localizedDescription)")
            return
        }
        
        /// Install an audio tap on the audio engine's input node.
        self.audioEngine.inputNode.installTap(onBus: 0,
                                              bufferSize: 8192, // 8k buffer
        format: inputFormat) { buffer, time in
            /// Analyze the current audio buffer.
            self.analysisQueue.async {
                self.streamAnalyzer.analyze(buffer, atAudioFramePosition: time.sampleTime)
            }
        }
    }
    
    
    // Function to Start Audio Engine for Recording Audio
    @objc func startAudioEngine() {
        do {
            /// Start the stream of audio data.
            try self.audioEngine.start()
            
        } catch {
            print("Unable to start AVAudioEngine: \(error.localizedDescription)")
        }
    }
    
    // Function to Stop Audio Engine for Recording Audio
    func stopAudioEngine() {
        /// Stop the stream of audio data.
        self.audioEngine.stop()
    }
    
    // Update resultsLabel function
    @objc func endDetection() {
        
        DispatchQueue.main.async {
            if self.isDetectionStarted {
                
                if (Session.sharedInstance.detectedSong != nil && !Session.sharedInstance.detectedSong.isEmpty && Session.sharedInstance.detectedSong != "***Noise***" && Session.sharedInstance.detectedSong != "Billie Eilish - bad guy") {
                    
                    /// Enabling User Interaction
                    self.spotifyButton.isUserInteractionEnabled = true
                    self.spotifyImage.isHidden = true
                    self.spotifyButton.isHidden = false
                    self.settingsButton.isHidden = false
                    self.xButton.isHidden = true
                    self.tableView.isHidden = false
                    self.tabBarController?.tabBar.isHidden = false
                    
                    /// Stop Pulsing
                    self.pulsator.save()
                    self.pulsator.stop()
                    
                    
                    /// Shazam Button Image Changing & Scaling + Shadow Configuration
                    self.spotifyImage.image = UIImage(named: "spotify")
                    self.spotifyImage.center = CGPoint(x: self.spotifyButton.center.x, y: self.spotifyButton.center.y)
                    let scaleTransform = CGAffineTransform(scaleX: 1 / 8.5, y: 1 / 8.5)
                    self.spotifyImage.transform = scaleTransform
                    self.spotifyImage.layer.shadowColor = UIColor.clear.cgColor
                    
                    /// Stop AudioEngine and Deletes Result String
                    self.stopAudioEngine()
                    
                    /// Presents DetectedSongViewController as modal
                    self.performSegue(withIdentifier: "detectedSongSegue", sender: self)
                    
                } else {
                    
                    /// Stop AudioEngine and Deletes Result String
                    self.stopAudioEngine()
                    
                    // Interrupt animations
                    self.pulsator.stop()
                    
                    // Unhide No Result Items
                    self.spotifyImage.isHidden = true
                    self.circle.isHidden = false
                    self.sorryLabel.isHidden = false
                    self.weDidntQuiteCatchThatLabel.isHidden = false
                    self.tryAgainButton.isHidden = false
                }
            }
        }
    }
    
    // HandleModalDismissed: Function called when SelectedSongViewController Modal is dismissed
    @objc func handleModalDismissed() {
        // Instruction Label reappearing and animating
        UIView.animate(withDuration: 2.0) {
            self.instructionsLabel.isHidden = false
            self.instructionsLabel.frame = CGRect(x: 53, y: 53, width: 143, height: 26)
            self.instructionsLabel.font = self.instructionsLabel.font.withSize(15)
            self.instructionsLabel.alpha = 1
        }
    }
    
}


// Extension to manage the TableView
extension HomeViewController: UITableViewDelegate, UITableViewDataSource, HomeCollectionViewCellSelectionDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            guard let homeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "homeTableViewCell", for: indexPath) as? HomeTableViewCell else {
                fatalError("Unable to create home table view cell")
            }
            
            homeTableViewCell.titleLabel.text = titles[indexPath.row]
            
            /// To set delegate for HomeCollectionViewCellSelectionDelegate found in the HomeTableViewCell class
            homeTableViewCell.delegate = self
            
            return homeTableViewCell
        } else {
            guard let homeTableViewCell2 = tableView.dequeueReusableCell(withIdentifier: "homeTableViewCell2", for: indexPath) as? HomeTableViewCell2 else {
                fatalError("Unable to create home table view cell")
            }
            
            homeTableViewCell2.titleLabel.text = titles[indexPath.row]
            
            /// To set delegate for HomeCollectionViewCellSelectionDelegate found in the HomeTableViewCell class
            homeTableViewCell2.delegate = self
            
            return homeTableViewCell2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 250
        } else {
            return 300
        }
    }
    
    /// Function to evoke segue, called by the HomeCollectionViewCellSelectionDelegate protocol in HomeTableViewCell class
    func selectedItem() {
        performSegue(withIdentifier: "showDetailsSegue", sender: self)
    }
    
}


// Observer object that is called as analysis results are found.
class ResultsObserver : NSObject, SNResultsObserving {
    
    var count: Int = 0
    var isDetectionStarted: Bool = true
    var lastTime: Double = 0
    var stringBuffer: String = ""
    
    func request(_ request: SNRequest, didProduce result: SNResult) {
        
        /// Get the top classification.
        guard let result = result as? SNClassificationResult,
            let classification = result.classifications.first else { return }
        
        /// Resetting the time at each detection
        if isDetectionStarted == true {
            lastTime = result.timeRange.start.seconds
            isDetectionStarted = false
        }
        
        /// Determine the time of this result.
        let formattedTime = String(format: "%.2f", result.timeRange.start.seconds - lastTime)
        print("Analysis result for audio at time: \(formattedTime)")
        
        let confidence = classification.confidence * 100.0
        let percent = String(format: "%.2f%%", confidence)
        
        /// Print the result as Instrument: percentage confidence.
        print("\(classification.identifier): \(percent) confidence.\n")

        
        if (stringBuffer == classification.identifier && classification.identifier != "Billie Eilish - bad guy" && classification.identifier != "***Noise***" && confidence >= 92.0) {
            count += 1
        } else if (stringBuffer != classification.identifier && classification.identifier != "Billie Eilish - bad guy" && classification.identifier != "***Noise***" && confidence >= 95.0) {
            count = 0
            stringBuffer = classification.identifier
        }
        
        if (count == 4) {
            Session.sharedInstance.detectedSong = stringBuffer
            stringBuffer = ""
            count = 0
            isDetectionStarted = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SongIsDetected"), object: nil)
        } else if (result.timeRange.start.seconds - lastTime > 15.0) {
            Session.sharedInstance.detectedSong = ""
            stringBuffer = ""
            count = 0
            isDetectionStarted = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SongIsDetected"), object: nil)
        }
    }
    
    func request(_ request: SNRequest, didFailWithError error: Error) {
        print("The the analysis failed: \(error.localizedDescription)")
    }
    
    func requestDidComplete(_ request: SNRequest) {
        print("The request completed successfully!")
    }
}

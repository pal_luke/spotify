//
//  ShowDetailsViewController.swift
//  Spotify
//
//  Created by Luca Palmese on 10/12/2019.
//  Copyright © 2019 Luca Palmese. All rights reserved.
//

import UIKit

class ShowDetailsViewController: UIViewController {
    
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        coverImage.image = Session.sharedInstance.selectedSong!.cover
        trackName.text = Session.sharedInstance.selectedSong!.title
        artistName.text = "Single by " + Session.sharedInstance.selectedSong!.artist

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

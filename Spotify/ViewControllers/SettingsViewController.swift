//
//  SettingsViewController.swift
//  Spotify
//
//  Created by Luca Palmese on 05/12/2019.
//  Copyright © 2019 Luca Palmese. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    // Text Attribute to make the NavigationBar title white
    let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden =  false
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

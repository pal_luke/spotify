//
//  DetectedSongViewController.swift
//  Spotify
//
//  Created by Luca Palmese on 12/12/2019.
//  Copyright © 2019 Luca Palmese. All rights reserved.
//

import UIKit

class DetectedSongViewController: UIViewController {
    
    
    @IBOutlet weak var displayingView: UIImageView!
    @IBOutlet weak var detectedSongCover: UIImageView!
    @IBOutlet weak var detectedSongTitle: UILabel!
    @IBOutlet weak var detectedSongArtist: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// Round corners to the view at the bottom
        displayingView.layer.cornerRadius = 10.0
        
        switch Session.sharedInstance.detectedSong {
        case "Agnes Obel - Familiar":
            detectedSongCover.image = UIImage(named: "familiar.jpg")
            detectedSongTitle.text = "Familiar"
            detectedSongArtist.text = "Agnes Obel"
            break
        case "Billie Eilish - bad guy":
            detectedSongCover.image = UIImage(named: "badguy.jpg")
            detectedSongTitle.text = "bad guy"
            detectedSongArtist.text = "Billie Eilish"
            break
        case "Imagine Dragons - Radioactive":
            detectedSongCover.image = UIImage(named: "radioactive.jpg")
            detectedSongTitle.text = "Radioactive"
            detectedSongArtist.text = "Imagine Dragons"
            break
        case "Lana Del Rey - Doin' Time":
            detectedSongCover.image = UIImage(named: "dointime.jpg")
            detectedSongTitle.text = "Doin' Time"
            detectedSongArtist.text = "Lana Del Rey"
            break
        case "Elodie & Marracash - Margarita":
            detectedSongCover.image = UIImage(named: "margarita.jpg")
            detectedSongTitle.text = "Margarita"
            detectedSongArtist.text = "Elodie & Marracash"
            break
        case "Mark Ronson - Late Night Feelings":
            detectedSongCover.image = UIImage(named: "latenightfeelings.jpg")
            detectedSongTitle.text = "Late Night Feelings"
            detectedSongArtist.text = "Mark Ronson & Lykke Li"
            break
        case "Post Malone - Circles":
            detectedSongCover.image = UIImage(named: "circles.jpg")
            detectedSongTitle.text = "Circles"
            detectedSongArtist.text = "Post Malone"
            break
        case "Steve Aoki - Are You Lonely":
            detectedSongCover.image = UIImage(named: "areyoulonely.jpg")
            detectedSongTitle.text = "Are You Lonely"
            detectedSongArtist.text = "Steve Aoki & Alan Walker"
            break
        case "Tove Lo - Sweettalk my Heart":
            detectedSongCover.image = UIImage(named: "sweettalkmyheart.jpg")
            detectedSongTitle.text = "Sweettalk my Heart"
            detectedSongArtist.text = "Tove Lo"
            break
        case "twenty one pilots - My Blood":
            detectedSongCover.image = UIImage(named: "myblood.png")
            detectedSongTitle.text = "My Blood"
            detectedSongArtist.text = "twenty one pilots"
            break
        case "Jess Glynne - I'll Be There":
            detectedSongCover.image = UIImage(named: "illbethere.jpg")
            detectedSongTitle.text = "I'll Be There"
            detectedSongArtist.text = "Jess Glynne"
            break
        case "Takagi & Ketra - Amore e Capoeira (feat. Giusy Ferreri)":
            detectedSongCover.image = UIImage(named: "amoreecapoeira.jpg")
            detectedSongTitle.text = "Amore e Capoeira (feat. Giusy Ferreri)"
            detectedSongArtist.text = "Takagi & Ketra"
            break
        case "Corona - The Rhythm Of The Night":
            detectedSongCover.image = UIImage(named: "therhythmofthenight.jpg")
            detectedSongTitle.text = "The Rhythm Of The Night"
            detectedSongArtist.text = "Corona"
            break
        case "Mabel - Don't Call Me Up":
            detectedSongCover.image = UIImage(named: "dontcallmeup.jpg")
            detectedSongTitle.text = "Don't Call Me Up"
            detectedSongArtist.text = "Mabel"
            break
        case "Shawn Mendes & Camila Cabello - Señorita":
            detectedSongCover.image = UIImage(named: "senorita.jpg")
            detectedSongTitle.text = "Señorita"
            detectedSongArtist.text = "Shawn Mendes & Camila Cabello"
            break
            
        default:
            detectedSongTitle.text = "mammt"
            detectedSongArtist.text = ""
        }
        
        // Resets Detected Song String
        Session.sharedInstance.detectedSong = ""

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DetectedSongModalIsDismissed"), object: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

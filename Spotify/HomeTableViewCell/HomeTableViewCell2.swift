//
//  HomeTableViewCell.swift
//  Spotify
//
//  Created by Luca Palmese on 08/12/2019.
//  Copyright © 2019 Luca Palmese. All rights reserved.
//

import UIKit

class HomeTableViewCell2: UITableViewCell {
    
    // TableView Cell's Label Outlet
    @IBOutlet weak var titleLabel: UILabel!
    
    // CollectionView Outlet
    @IBOutlet weak var collectionView2: UICollectionView!
    
    // Defining songs array
    var songs: [Song] = [Song(cover: UIImage(named: "circles.jpg")!, title: "Circles", artist: "Post Malone"), Song(cover: UIImage(named: "areyoulonely.jpg")!, title: "Are You Lonely?", artist: "Steve Aoki & Alan Walker"), Song(cover: UIImage(named: "latenightfeelings.jpg")!, title: "Late Night Feelings", artist: "Mark Ronson & Lykke Li"), Song(cover: UIImage(named: "margarita.jpg")!, title: "Margarita", artist: "Elodie & Marracash"), Song(cover: UIImage(named: "radioactive.jpg")!, title: "Radioactive", artist: "Imagine Dragons"), Song(cover: UIImage(named: "familiar.jpg")!, title: "Familiar", artist: "Agnes Obel"), Song(cover: UIImage(named: "myblood.png")!, title: "My Blood", artist: "twenty one pilots"), Song(cover: UIImage(named: "sweettalkmyheart.jpg")!, title: "Sweettalk My Heart", artist: "Tove Lo"), Song(cover: UIImage(named: "dointime.jpg")!, title: "Doin' Time", artist: "Lana Del Rey"), Song(cover: UIImage(named: "illbethere.jpg")!, title: "I'll Be There", artist: "Jess Glynne"), Song(cover: UIImage(named: "therhythmofthenight.jpg")!, title: "The Rhythm Of The Night", artist: "Corona"), Song(cover: UIImage(named: "senorita.jpg")!, title: "Señorita", artist: "Shawn Mendes & Camila Cabello"), Song(cover: UIImage(named: "amoreecapoeira.jpg")!, title: "Amore e Capoeira (feat. Giusy Ferreri)", artist: "Takagi & Ketra"), Song(cover: UIImage(named: "dontcallmeup.jpg")!, title: "Don't Call Me Up", artist: "Mabel")]
    
    // Delegate to select HomeCollectionViewCell
    weak var delegate: HomeCollectionViewCellSelectionDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Giving CollectionView the HomeTableViewCell as Delegate and DataSource
        collectionView2.delegate = self
        collectionView2.dataSource = self
        
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


// HomeTableViewCell extension to support the CollectionView
extension HomeTableViewCell2: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let homeCollectionViewCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCollectionViewCell2", for: indexPath) as? HomeCollectionViewCell2 else {fatalError("Unable to create a CollectionView cell")}
        
        let random = Int.random(in: 0..<14)
        
        homeCollectionViewCell2.coverImage.image = songs[random].cover
        homeCollectionViewCell2.trackName.text = songs[random].title
        homeCollectionViewCell2.artistName.text = songs[random].artist
        
        return homeCollectionViewCell2
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedItem = collectionView.cellForItem(at: indexPath) as! HomeCollectionViewCell2
        
        Session.sharedInstance.selectedSong = Song(cover: selectedItem.coverImage.image!, title: selectedItem.trackName.text!, artist: selectedItem.artistName.text!)
        
        self.delegate?.selectedItem()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 166, height: 316)
    }
}

//
//  HomeCollectionViewCell.swift
//  Spotify
//
//  Created by Luca Palmese on 09/12/2019.
//  Copyright © 2019 Luca Palmese. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    // Song's Cover ImageView Outlet
    @IBOutlet weak var coverImage: UIImageView!
    
    // Song's TrackName Label Outlet
    @IBOutlet weak var trackName: UILabel!
    
    // Song's ArtistName Label Outlet
    @IBOutlet weak var artistName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

//
//  Session.swift
//  Spotify
//
//  Created by Luca Palmese on 12/12/2019.
//  Copyright © 2019 Luca Palmese. All rights reserved.
//

import Foundation

class Session {
    
    static public var sharedInstance = Session()
    
    var selectedSong: Song?
    var detectedSong: String!
    
    // Init
    private init() {}
    
}

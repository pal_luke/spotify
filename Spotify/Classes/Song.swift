//
//  Song.swift
//  Spotify
//
//  Created by Luca Palmese on 09/12/2019.
//  Copyright © 2019 Luca Palmese. All rights reserved.
//

import Foundation
import UIKit

class Song {
    
    var cover: UIImage!
    var title: String!
    var artist: String!
    
    init(cover: UIImage, title: String, artist: String) {
        self.cover = cover
        self.title = title
        self.artist = artist
    }
}
